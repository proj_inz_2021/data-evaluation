## Data evaluation scripts
### Set up
Create folder 'devices_results' and copy results from tested devices there. 

Sample input should look like this
```
device_results
└───device_1
│   └───model_1
│   │   └───test_1
│   │   │   │ delays.json
│   │   │   └───boxes
│   │   │       │   00000001.txt
│   │   │       │   ...
│   │   │   ...  
│   │   ...
│   ...
```

### Scripts
**calculate_map.py** - calculates mAP (mean average precision) for every performed test 
and save output in test directory (device_results/_device_/_model_/_test_)

**create_csv.py** - creates csv table that summarizes the performance of each model on each device 

**parse_to_txt.py** - change extension of box files to .txt

**correct_boxes.py** - corrects the order of the coordinates in box files _ in 
double-barrelled class names

**main.py** - runs the above scripts in the correct order
 
