import os
import shutil

from src.file_support import iterate_over_tests


DETECTION_RESULTS_PATH = 'src/mAP/input/detection-results'
MAP_OUTPUT_PATH = 'src/mAP/output/'


def calculate_map(override=True):
    for test_path in iterate_over_tests():
        boxes_path = os.path.join(test_path, 'boxes')
        if os.path.isdir(DETECTION_RESULTS_PATH):
            shutil.rmtree(DETECTION_RESULTS_PATH)

        shutil.copytree(boxes_path, DETECTION_RESULTS_PATH)

        print(test_path)
        map_destination = os.path.join(test_path, 'mAP')

        if override or not os.path.exists(map_destination):
            os.system("python src/mAP/mAP.py -na -np -q")

            if os.path.isdir(map_destination):
                shutil.rmtree(map_destination)
            shutil.copytree(MAP_OUTPUT_PATH, map_destination)


if __name__ == '__main__':
    calculate_map()
