import numpy as np
from plotnine import ggplot, aes, geom_histogram, geom_vline, annotate, xlab, ylab, theme_bw, theme_set

from src.dataframe import create_dataframe
from src.file_support import save_gg_plot, save_json

theme_set(theme_bw(base_size=18))


def calculate_statistics():
    main_df = create_dataframe()
    statistics = {
        # Delay
        'min_delay': np.min(list(main_df['delay'])),
        'max_delay': np.max(list(main_df['delay'])),
        'mean_delay': np.mean(list(main_df['delay'])),
        'std_delay': np.std(list(main_df['delay'])),

        # mAP
        'min_mAP': np.min(list(main_df['mAP'])),
        'max_mAP': np.max(list(main_df['mAP'])),
        'mean_mAP': np.mean(list(main_df['mAP'])),
        'std_mAP': np.std(list(main_df['mAP']))
    }
    save_json(statistics, 'statistics')

    # Delay histogram
    delay_histogram = ggplot(main_df)
    delay_histogram += geom_vline(xintercept=statistics['mean_delay'], colour="green", linetype="dashed")
    delay_histogram += annotate(geom="text", label='μ = %s' % round(statistics['mean_delay']), x=(statistics['mean_delay'] + 350), y=50, angle=90, colour="green")

    mean_1std = round(statistics['mean_delay'] + statistics['std_delay'])
    delay_histogram += geom_vline(xintercept=mean_1std, colour="tomato", linetype="dashed")
    delay_histogram += annotate(geom="text", label='μ + 1σ = %s' % mean_1std, x=(mean_1std + 350), y=50, angle=90, colour="tomato")

    mean_2std = round(statistics['mean_delay'] + 2 * statistics['std_delay'])
    delay_histogram += geom_vline(xintercept=mean_2std, colour="orangered", linetype="dashed")
    delay_histogram += annotate(geom="text", label='μ + 2σ = %s' % mean_2std, x=(mean_2std + 350), y=50, angle=90, colour="orangered")

    mean_3std = round(statistics['mean_delay'] + 3 * statistics['std_delay'])
    delay_histogram += geom_vline(xintercept=mean_3std, colour="red", linetype="dashed")
    delay_histogram += annotate(geom="text", label='μ + 3σ = %s' % mean_3std, x=(mean_3std + 350), y=50, angle=90, colour="red")

    delay_histogram += geom_histogram(aes(x='delay'))
    delay_histogram += xlab("Czas inferencji [ms]")
    delay_histogram += ylab("Ilość wyników")
    delay_histogram += theme_bw()

    save_gg_plot(delay_histogram, filename='0_delay_histogram')


if __name__ == '__main__':
    calculate_statistics()
