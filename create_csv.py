import csv
import os
from pathlib import Path
from typing import Union, Dict, List

import numpy as np

from src.file_support import load_txt_file, load_json_file


BASE_PATH = 'devices_results'
OUTPUT_PATH = 'output/csv'


def calculate_delay_from_file(file_path: Union[str, os.PathLike]) -> float:
    delays_json = load_json_file(file_path)
    delays_np = np.array([elem['time'] for elem in delays_json])

    mean_delay = delays_np.mean()
    median_delay = np.median(delays_np)
    _3sigma = np.std(delays_np)

    three_sigma_mean = np.array([delay for delay in delays_np if abs(delay - mean_delay) < _3sigma]).mean()

    # print(list(sorted(list(enumerate([mean_delay, median_delay, three_sigma_mean])), key=lambda x: x[1])))
    return median_delay


def get_map_from_file(file_path: Union[str, os.PathLike]) -> float:
    map_data = load_txt_file(file_path)
    map_string = "mAP = "
    found_map_place = map_data.find(map_string)
    found_percentage_place = map_data[found_map_place:].find('%')
    assert found_map_place != -1
    return float(map_data[found_map_place + len(map_string):found_map_place + found_percentage_place])


def save_csv(models: List[str], devices: List[str], results_data: Dict, option: int):
    rows = [["Device"] + models]
    for device in devices:
        row = [device]
        for model in models:
            result = str(results_data[device, model][option] if (device, model) in results_data.keys() else "None")
            row.append(result.replace('.', ','))
        rows.append(row)

    output_names = {
        0: "delays.csv",
        1: "mAP.csv"
    }

    Path(OUTPUT_PATH).mkdir(parents=True, exist_ok=True)
    with open(os.path.join(OUTPUT_PATH, output_names[option]),  'w', newline='', encoding='UTF8') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerows(rows)


def create_csv():
    model_names = set()
    device_names = set()
    result_dict = {}

    for i, device in enumerate(os.listdir(BASE_PATH)):
        no_devices = len(os.listdir(BASE_PATH))
        device_path = os.path.join(BASE_PATH, device)
        device_names.add(device)

        for j, model in enumerate(os.listdir(device_path)):
            no_models = len(os.listdir(device_path))
            model_path = os.path.join(device_path, model)
            model_names.add(model)

            model_delays = []
            model_map_list = []

            for test in os.listdir(model_path):
                test_path = os.path.join(model_path, test)
                model_delays.append(calculate_delay_from_file(os.path.join(test_path, 'delays.json')))
                model_map_list.append(get_map_from_file(os.path.join(os.path.join(test_path, 'mAP'), 'output.txt')))

            result_dict[(device, model)] = np.array(model_delays).mean(), np.array(model_map_list).mean()

            print("Device %s/%s, Model %s/%s" % (i + 1, no_devices, j + 1, no_models))

    model_names = sorted(list(model_names))
    device_names = sorted(list(device_names))
    save_csv(model_names, device_names, result_dict, 0)
    save_csv(model_names, device_names, result_dict, 1)


if __name__ == '__main__':
    create_csv()
