from typing import Tuple

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from src.file_support import load_csv, save_plt_heatmap

sns.set_theme()
sns.set(rc={'figure.figsize': (11.7 * 2.4, 8.27 * 1.5)})
# sns.set(rc={'figure.figsize': (8.27 * 2.3, 11.7 * 1.8)})


COLOR_MAP_R = sns.color_palette(palette="RdYlGn_r", as_cmap=True)
COLOR_MAP = sns.color_palette(palette="RdYlGn", as_cmap=True)


def create_heatmaps():
    delays_df, map_df = create_2d_dataframes()

    # Overall heatmaps
    sns.heatmap(delays_df, annot=True, vmin=10, center=350, vmax=700, cmap=COLOR_MAP_R, fmt='g')
    save_plt_heatmap('heatmap_delay')
    plt.show()

    sns.heatmap(map_df, annot=True, vmin=10, center=35, vmax=60, cmap=COLOR_MAP, fmt='g')
    save_plt_heatmap('heatmap_mAP')
    plt.show()

    # One Plus CPU heatmaps
    # sns.set(rc={'figure.figsize': (11.7 * 1, 8.27 * 0.05)})
    # default_models = [model for model in delays_df.columns if 'no_optimization' not in model and 'quant' not in model]
    # mobilenet_models = default_models
    # one_plus_cpu_df = delays_df[delays_df.index.str.startswith('OnePlus-9_CPU')]
    # sns.heatmap(one_plus_cpu_df, annot=True, vmin=10, center=350, vmax=700, cmap=COLOR_MAP_R, fmt='g')
    # save_plt_heatmap('Mobilenet_One_Plus_CPU')
    # plt.show()


def create_2d_dataframes() -> Tuple[pd.DataFrame, pd.DataFrame]:
    delays_models, delays_devices, delays_data = load_csv('delays.csv', none_value=np.nan)
    map_models, map_devices, map_data = load_csv('mAP.csv', none_value=np.nan)

    models = list(map(lambda x: x.split('.')[0], map_models))

    delays_data = np.around(delays_data, decimals=0)
    delays_dataframe = pd.DataFrame(data=delays_data, index=delays_devices, columns=models)
    map_dataframe = pd.DataFrame(data=map_data, index=map_devices, columns=models)

    return sort_by_device(delays_dataframe), sort_by_device(map_dataframe)


def sort_by_device(df: pd.DataFrame):
    df['device_type'] = [0 if 'Coral' in device else i + 1 * len(list(df.index)) if 'Raspberry' in device else i + 2 * len(list(df.index)) for i, device in enumerate(list(df.index))]
    df = df.sort_values(by=["device_type"])
    df = df.drop(columns=["device_type"])
    return df


if __name__ == '__main__':
    create_heatmaps()
