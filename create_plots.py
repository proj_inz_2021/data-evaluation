
import numpy as np
import pandas as pd
import plotnine as p9

from src.file_support import save_gg_plot, load_csv
from src.dataframe import create_dataframe, split_dataframe, remove_mobilenet_v1, get_pareto_optimal

p9.theme_set(p9.theme_bw(base_size=18))


def prepare_plot(plot: p9.ggplot, filename: str, x_lab: str, y_lab: str, angle: int = 0):
    plot += p9.theme(axis_text_x=p9.element_text(angle=angle, hjust=(90 - angle / 2) / 90))

    if x_lab is not None:
        plot += p9.xlab(x_lab)

    if y_lab is not None:
        plot += p9.ylab(y_lab)

    save_gg_plot(plot, filename)


def create_col_plot(df: pd.DataFrame, x: str, y: str, filename: str, x_lab: str, y_lab: str, angle: int = 0, y_lim=None,
                    order=None):
    col_plot = p9.ggplot(df, p9.aes(x=x, y=y)) + p9.geom_col(fill="steelblue")
    if y_lim is not None:
        col_plot += p9.coord_cartesian(ylim=(y_lim[0], y_lim[1]))
    if order is not None:
        col_plot += p9.scale_x_discrete(limits=order)
    prepare_plot(col_plot, filename, x_lab, y_lab, angle)


def create_scatter_plot(df: pd.DataFrame, x: str, y: str, color: str, filename: str, x_lab: str, y_lab: str,
                        angle: int = 0, legend=None):
    plot = p9.ggplot(df, p9.aes(x=x, y=y, color=color)) + p9.geom_point(alpha=0.8)
    if legend is not None:
        plot += p9.scale_colour_discrete(name=legend)
    prepare_plot(plot, filename, x_lab, y_lab, angle)


def section0_plots(df_dict):
    default_models_df = df_dict['default_models'].sort_values(by=["device_type"])

    # Default model scatter colored by model
    create_scatter_plot(default_models_df, 'delay', 'mAP', 'model', '0_def_models_scatter_by_model',
                        'Czas inferencji [ms]', 'mAP [%]', legend="Model sieci")

    # Default model scatter colored by architecture
    create_scatter_plot(default_models_df, 'delay', 'mAP', 'architecture', '0_def_models_scatter_by_architecture',
                        'Czas inferencji [ms]', 'mAP [%]', legend="Architektura modelu")

    # Default model scatter colored by device
    create_scatter_plot(default_models_df, 'delay', 'mAP', 'device', '0_def_models_scatter_by_device',
                        'Czas inferencji [ms]', 'mAP [%]', legend="Urządzenie")

    # Default model scatter colored by device_type
    create_scatter_plot(default_models_df, 'delay', 'mAP', 'device_type', '0_def_models_scatter_by_device_type',
                        'Czas inferencji [ms]', 'mAP [%]', legend="Typ urządzenia")

    # Default model column delay by model
    default_models_df_grouped_by_device = default_models_df.groupby(['model']).mean()
    default_models_df_grouped_by_device.reset_index(level=0, inplace=True)
    create_col_plot(default_models_df_grouped_by_device, 'model', 'delay', '0_every_models_delay', 'Urządzenie',
                    'Czas inferencji [ms]', angle=90)

    default_models_df_grouped_by_models = default_models_df.groupby(['device']).mean()
    default_models_df_grouped_by_models.reset_index(level=0, inplace=True)
    create_col_plot(default_models_df_grouped_by_models, 'device', 'delay', '0_every_device_delay', 'Urządzenie',
                    'Czas inferencji [ms]', angle=90)


def section1_plots(df_dict):
    # Mobilenet model versions
    one_plus_cpu_mobilenet_df = df_dict['one_plus_cpu_mobilenet']
    sorted_models = list(one_plus_cpu_mobilenet_df.sort_values(by=["delay"])['model'])
    # one_plus_cpu_mobilenet_df["order"] = [i for i in range(len(one_plus_cpu_mobilenet_df.index.to_list()))]
    # one_plus_cpu_mobilenet_df = one_plus_cpu_mobilenet_df.set_index('order')

    create_col_plot(one_plus_cpu_mobilenet_df, 'model', 'delay', '1_mobilenet_delay_OnePlus_CPU', 'Model',
                    'Czas inferencji [ms]', angle=90, order=sorted_models)
    create_col_plot(one_plus_cpu_mobilenet_df, 'model', 'mAP', '1_mobilenet_mAP_OnePlus_CPU', 'Model', 'mAP [%s]',
                    angle=90, y_lim=(20, 70), order=sorted_models)

    one_plus_gpu_mobilenet_df = df_dict['one_plus_gpu_mobilenet']
    create_col_plot(one_plus_gpu_mobilenet_df, 'model', 'delay', '1_mobilenet_delay_OnePlus_GPU', 'Model',
                    'Czas inferencji [ms]', angle=90, order=sorted_models)
    create_col_plot(one_plus_gpu_mobilenet_df, 'model', 'mAP', '1_mobilenet_mAP_OnePlus_GPU', 'Model', 'mAP [%s]',
                    angle=90, y_lim=(20, 70), order=sorted_models)

    one_plus_mobilenet_delay_df = p9.ggplot(df_dict['one_plus_mobilenet'], p9.aes(x='model', y='delay'))
    one_plus_mobilenet_delay_df += p9.geom_col(p9.aes(fill='process_unit'), stat='identity', position='dodge')
    one_plus_mobilenet_delay_df += p9.scale_fill_discrete(name=" ")
    one_plus_mobilenet_delay_df += p9.scale_x_discrete(limits=sorted_models)
    prepare_plot(one_plus_mobilenet_delay_df, '1_mobilenet_delay_OnePlus', 'Model', 'Czas inferencji [ms]', angle=90)

    one_plus_mobilenet_map_df = p9.ggplot(df_dict['one_plus_mobilenet'], p9.aes(x='model', y='mAP'))
    one_plus_mobilenet_map_df += p9.geom_col(p9.aes(fill='process_unit'), stat='identity', position='dodge')
    one_plus_mobilenet_map_df += p9.scale_fill_discrete(name=" ")
    one_plus_mobilenet_map_df += p9.scale_x_discrete(limits=sorted_models)
    prepare_plot(one_plus_mobilenet_map_df, '1_mobilenet_mAP_OnePlus', 'Model', 'mAP [%s]', angle=90)


def section2_plots(df_dict):
    # Efficientdet model versions
    one_plus_cpu_efficientdet_df = df_dict['one_plus_cpu_efficientdet']
    create_col_plot(one_plus_cpu_efficientdet_df, 'model', 'delay', '2_efficientdet_delay_OnePlus_CPU', 'Model',
                    'Czas inferencji [ms]', angle=90)
    create_col_plot(one_plus_cpu_efficientdet_df, 'model', 'mAP', '2_efficientdet_mAP_OnePlus_CPU', 'Model', 'mAP [%]',
                    y_lim=(20, 70), angle=90)

    one_plus_gpu_efficientdet_df = df_dict['one_plus_gpu_mobilenet']
    create_col_plot(one_plus_gpu_efficientdet_df, 'model', 'delay', '2_efficientdet_delay_OnePlus_GPU', 'Model',
                    'Czas inferencji [ms]', angle=90)
    create_col_plot(one_plus_gpu_efficientdet_df, 'model', 'mAP', '2_efficientdet_mAP_OnePlus_GPU', 'Model', 'mAP [%s]',
                    angle=90, y_lim=(20, 70))

    one_plus_mobilenet_delay_df = p9.ggplot(df_dict['one_plus_efficientdet'], p9.aes(x='model', y='delay'))
    one_plus_mobilenet_delay_df += p9.geom_col(p9.aes(fill='process_unit'), stat='identity', position='dodge')
    one_plus_mobilenet_delay_df += p9.scale_fill_discrete(name=" ")
    prepare_plot(one_plus_mobilenet_delay_df, '2_efficientdet_delay_OnePlus', 'Model', 'Czas inferencji [ms]', angle=90)

    one_plus_mobilenet_map_df = p9.ggplot(df_dict['one_plus_efficientdet'], p9.aes(x='model', y='mAP'))
    one_plus_mobilenet_map_df += p9.geom_col(p9.aes(fill='process_unit'), stat='identity', position='dodge')
    one_plus_mobilenet_map_df += p9.scale_fill_discrete(name=" ")
    prepare_plot(one_plus_mobilenet_map_df, '2_efficientdet_mAP_OnePlus', 'Model', 'mAP [%s]', angle=90)


def section3_plots(df_dict):
    one_plus_def_cpu_df = df_dict['one_plus_cpu_def']
    delay_values = list(one_plus_def_cpu_df['delay'])
    map_values = list(map(lambda x: (x / 100) * 300, one_plus_def_cpu_df['mAP']))
    values = map_values + delay_values
    keys = ['mAP'] * len(one_plus_def_cpu_df['mAP']) + ['Czas inferencji'] * len(one_plus_def_cpu_df['delay'])
    models = list(one_plus_def_cpu_df['model']) * 2
    data = list(zip(models, keys, values))

    map_delay_df = pd.DataFrame(data=data, columns=['model', 'key', 'value'])
    model_values_bar_plot = p9.ggplot(map_delay_df, p9.aes(x='model', y='value'))
    model_values_bar_plot += p9.geom_col(p9.aes(fill='key'), stat='identity', position='dodge')

    model_values_bar_plot += p9.scale_fill_discrete(name=" ")
    model_values_bar_plot += p9.coord_cartesian(ylim=(None, 350))

    prepare_plot(model_values_bar_plot, '3_eff_vs_mob_OnePlus_CPU', x_lab="Model", y_lab="Czas inferencji [ms]", angle=90)

    one_plus_def_gpu_df = df_dict['one_plus_gpu_def']
    delay_values = list(one_plus_def_gpu_df['delay'])
    map_values = list(map(lambda x: (x / 100) * 300, one_plus_def_gpu_df['mAP']))
    values = map_values + delay_values
    keys = ['mAP'] * len(one_plus_def_gpu_df['mAP']) + ['Czas inferencji'] * len(one_plus_def_gpu_df['delay'])
    models = list(one_plus_def_gpu_df['model']) * 2
    data = list(zip(models, keys, values))

    map_delay_df = pd.DataFrame(data=data, columns=['model', 'key', 'value'])
    model_values_bar_plot = p9.ggplot(map_delay_df, p9.aes(x='model', y='value'))
    model_values_bar_plot += p9.geom_col(p9.aes(fill='key'), stat='identity', position='dodge')

    model_values_bar_plot += p9.scale_fill_discrete(name=" ")
    model_values_bar_plot += p9.coord_cartesian(ylim=(None, 350))

    prepare_plot(model_values_bar_plot, '3_eff_vs_mob_OnePlus_GPU', x_lab="Model", y_lab="Czas inferencji [ms]", angle=90)


def section4_plots(df_dict):
    # Input size comparison V1
    size_compare_df = df_dict['default_mobilenet_v1_size_compare']
    delay_mean_320 = round(
        np.mean(list(size_compare_df.loc[size_compare_df['model'] == 'mobilenet-v1-300x300']['delay'])), 2)
    delay_mean_640 = round(
        np.mean(list(size_compare_df.loc[size_compare_df['model'] == 'mobilenet-v1-fpn-640x640']['delay'])), 2)

    input_v1_size_comparison = p9.ggplot(size_compare_df, p9.aes(x='device', y='delay'))
    input_v1_size_comparison += p9.geom_col(p9.aes(fill='model'), stat='identity', position='dodge')
    input_v1_size_comparison += p9.scale_fill_discrete(name="Model")

    input_v1_size_comparison += p9.geom_hline(yintercept=delay_mean_320, colour="firebrick", linetype="dashed")
    input_v1_size_comparison += p9.annotate(geom="text", label='μ = %s' % delay_mean_320, x=3, y=(delay_mean_320 + 300),
                                            colour="firebrick")

    input_v1_size_comparison += p9.geom_hline(yintercept=delay_mean_640, colour='darkblue', linetype="dashed")
    input_v1_size_comparison += p9.annotate(geom="text", label='μ = %s' % delay_mean_640, x=3, y=(delay_mean_640 + 300),
                                            colour="darkblue")

    prepare_plot(input_v1_size_comparison, '4_v1_input_size_comparison_delay', "Urządzenie", 'Czas inferencji [ms]',
                 angle=90)

    input_v1_size_comparison += p9.scale_y_log10()

    prepare_plot(input_v1_size_comparison, '4_v1_input_size_comparison_log_delay', "Urządzenie", 'Czas inferencji [ms]',
                 angle=90)

    map_mean_320 = round(
        np.mean(list(size_compare_df.loc[size_compare_df['model'] == 'mobilenet-v1-300x300']['mAP'])), 2)
    map_mean_640 = round(
        np.mean(list(size_compare_df.loc[size_compare_df['model'] == 'mobilenet-v1-fpn-640x640']['mAP'])), 2)

    input_v1_size_comparison_map = p9.ggplot(size_compare_df, p9.aes(x='device', y='mAP'))
    input_v1_size_comparison_map += p9.geom_col(p9.aes(fill='model'), stat='identity', position='dodge')
    input_v1_size_comparison_map += p9.scale_fill_discrete(name="Model")

    input_v1_size_comparison_map += p9.geom_hline(yintercept=map_mean_320, colour="firebrick", linetype="dashed")
    input_v1_size_comparison_map += p9.annotate(geom="text", label='μ = %s' % map_mean_320, x=6, y=(map_mean_320 - 1),
                                                colour="firebrick")

    input_v1_size_comparison_map += p9.geom_hline(yintercept=map_mean_640, colour='darkblue', linetype="dashed")
    input_v1_size_comparison_map += p9.annotate(geom="text", label='μ = %s' % map_mean_640, x=3, y=(map_mean_640 + 1),
                                                colour="darkblue")

    prepare_plot(input_v1_size_comparison_map, '4_v1_input_size_comparison_mAP', x_lab="Urządzenie", y_lab='mAP [%]',
                 angle=90)

    # Input size comparison V2
    size_compare_df = df_dict['default_mobilenet_v2_size_compare']
    delay_mean_320 = round(
        np.mean(list(size_compare_df.loc[size_compare_df['model'] == 'mobilenet-v2-fpnlite-320x320']['delay'])), 2)
    delay_mean_640 = round(
        np.mean(list(size_compare_df.loc[size_compare_df['model'] == 'mobilenet-v2-fpnlite-640x640']['delay'])), 2)

    input_v2_size_comparison = p9.ggplot(size_compare_df, p9.aes(x='device', y='delay'))
    input_v2_size_comparison += p9.geom_col(p9.aes(fill='model'), stat='identity', position='dodge')
    input_v2_size_comparison += p9.scale_fill_discrete(name="Model")

    input_v2_size_comparison += p9.geom_hline(yintercept=delay_mean_320, colour="firebrick", linetype="dashed")
    input_v2_size_comparison += p9.annotate(geom="text", label='μ = %s' % delay_mean_320, x=3, y=(delay_mean_320 + 100),
                                            colour="firebrick")

    input_v2_size_comparison += p9.geom_hline(yintercept=delay_mean_640, colour='darkblue', linetype="dashed")
    input_v2_size_comparison += p9.annotate(geom="text", label='μ = %s' % delay_mean_640, x=3, y=(delay_mean_640 + 100),
                                            colour="darkblue")

    prepare_plot(input_v2_size_comparison, '4_v2_input_size_comparison', "Urządzenie", 'Czas inferencji [ms]', angle=90)

    input_v2_size_comparison += p9.scale_y_log10()

    prepare_plot(input_v2_size_comparison, '4_v2_input_size_comparison_log', "Urządzenie", 'Czas inferencji [ms]',
                 angle=90)

    map_mean_320 = round(
        np.mean(list(size_compare_df.loc[size_compare_df['model'] == 'mobilenet-v2-fpnlite-320x320']['mAP'])), 2)
    map_mean_640 = round(
        np.mean(list(size_compare_df.loc[size_compare_df['model'] == 'mobilenet-v2-fpnlite-640x640']['mAP'])), 2)

    input_v2_size_comparison_map = p9.ggplot(size_compare_df, p9.aes(x='device', y='mAP'))
    input_v2_size_comparison_map += p9.geom_col(p9.aes(fill='model'), stat='identity', position='dodge')
    input_v2_size_comparison_map += p9.scale_fill_discrete(name="Model")

    input_v2_size_comparison_map += p9.geom_hline(yintercept=map_mean_320, colour="firebrick", linetype="dashed")
    input_v2_size_comparison_map += p9.annotate(geom="text", label='μ = %s' % map_mean_320, x=3.3, y=(map_mean_320 + 1),
                                                colour="firebrick")

    input_v2_size_comparison_map += p9.geom_hline(yintercept=map_mean_640, colour='darkblue', linetype="dashed")
    input_v2_size_comparison_map += p9.annotate(geom="text", label='μ = %s' % map_mean_640, x=3, y=(map_mean_640 + 1),
                                                colour="darkblue")

    prepare_plot(input_v2_size_comparison_map, '4_v2_input_size_comparison_map', "Urządzenie", "mAP [%]", angle=90)


def section5_plots(df_dict):
    # GPU vs CPU on OnePlus
    model_delay_OnePlus_CPUvsGPU = p9.ggplot(df_dict['one_plus_def'], p9.aes(x='model', y='delay'))
    model_delay_OnePlus_CPUvsGPU += p9.geom_col(p9.aes(fill='process_unit'), stat='identity', position='dodge')
    model_delay_OnePlus_CPUvsGPU += p9.scale_fill_discrete(name=" ")
    prepare_plot(model_delay_OnePlus_CPUvsGPU, '5_default_models_OnePlus_GPUvsCPU', 'Model', 'Czas inferencji [ms]',
                 angle=90)

    # GPU vs CPU on every device
    mean_delay_CPUvsGPU_df = df_dict['default_models_smartphones'].groupby(['device']).mean()
    mean_delay_CPUvsGPU_df.reset_index(level=0, inplace=True)
    mean_delay_CPUvsGPU_df['device_name'] = [device.split('_')[0] for device in mean_delay_CPUvsGPU_df['device']]
    mean_delay_CPUvsGPU_df['process_unit'] = ['CPU' if 'CPU' in device else 'GPU' for device in
                                              mean_delay_CPUvsGPU_df['device']]

    mean_delay_CPUvsGPU = p9.ggplot(mean_delay_CPUvsGPU_df, p9.aes(x='device_name', y='delay'))
    mean_delay_CPUvsGPU += p9.geom_col(p9.aes(fill='process_unit'), stat='identity', position='dodge')
    mean_delay_CPUvsGPU += p9.scale_fill_discrete(name=" ")
    prepare_plot(mean_delay_CPUvsGPU, '5_default_models_smartphones_GPUvsCPU', 'Model', 'Średni czas inferencji [ms]',
                 angle=90)


def section6_plots(df_dict):
    # One Plus CPU - comparison type of quantization
    one_plus_cpu_df = df_dict['one_plus_cpu']
    models_quantized = list(set(one_plus_cpu_df.loc[one_plus_cpu_df['quantization'] == 'Full integer']['model_name']))
    model_to_compare = [model in models_quantized for model in one_plus_cpu_df['model_name']]
    one_plus_cpu_df['to_compare'] = model_to_compare
    quantization_one_plus_cpu_df = one_plus_cpu_df.loc[one_plus_cpu_df['to_compare'] == True]

    quantization_cpu_delay = p9.ggplot(quantization_one_plus_cpu_df, p9.aes(x='quantization', y='delay'))
    quantization_cpu_delay += p9.geom_col(stat='identity')
    quantization_cpu_delay += p9.facet_wrap('model_name')
    prepare_plot(quantization_cpu_delay, '6_quantization_OnePlus_CPU_delay', "Rodzaj kwantyzacji",
                 "Czas inferencji [ms]", angle=90)

    quantization_cpu_map = p9.ggplot(quantization_one_plus_cpu_df, p9.aes(x='quantization', y='mAP'))
    quantization_cpu_map += p9.geom_col(stat='identity')
    quantization_cpu_map += p9.facet_wrap('model_name')
    prepare_plot(quantization_cpu_map, '6_quantization_OnePlus_CPU_mAP', "Rodzaj kwantyzacji", "mAP [%]", angle=90)

    # Same but without mobilenet v1 640x640
    quantization_one_plus_cpu_df_without_m1 = remove_mobilenet_v1(quantization_one_plus_cpu_df)

    quantization_cpu_delay_without_m1 = p9.ggplot(quantization_one_plus_cpu_df_without_m1,
                                                  p9.aes(x='quantization', y='delay'))
    quantization_cpu_delay_without_m1 += p9.geom_col(stat='identity')
    quantization_cpu_delay_without_m1 += p9.facet_wrap('model_name')
    prepare_plot(quantization_cpu_delay_without_m1, '6_quantization_OnePlus_CPU_delay_without_m1', "Rodzaj kwantyzacji",
                 "Czas inferencji [ms]", angle=90)

    quantization_cpu_map_without_m1 = p9.ggplot(quantization_one_plus_cpu_df_without_m1,
                                                p9.aes(x='quantization', y='mAP'))
    quantization_cpu_map_without_m1 += p9.geom_col(stat='identity')
    quantization_cpu_map_without_m1 += p9.facet_wrap('model_name')
    prepare_plot(quantization_cpu_map_without_m1, '6_quantization_OnePlus_CPU_mAP_without_m1', "Rodzaj kwantyzacji",
                 "mAP [%]", angle=90)


def section7_plots(df_dict):
    raspberry_pi_df = df_dict['raspberry_without_coral']

    # Raspberry PI - comparison 32bit and 64bit Czas inferencji
    delay_mean_32 = round(
        np.mean(list(raspberry_pi_df.loc[raspberry_pi_df['device'] == 'Raspberry-Pi-Rasbian-32bit']['delay'])), 2)
    delay_mean_64 = round(
        np.mean(list(raspberry_pi_df.loc[raspberry_pi_df['device'] == 'Raspberry-Pi-Ubuntu-64bit']['delay'])), 2)

    raspberry_pi_delay = p9.ggplot(raspberry_pi_df, p9.aes(x='model', y='delay'))
    raspberry_pi_delay += p9.geom_col(p9.aes(fill='device_name'), stat='identity', position='dodge')
    raspberry_pi_delay += p9.scale_fill_discrete(name="Model")

    raspberry_pi_delay += p9.geom_hline(yintercept=delay_mean_32, colour="firebrick", linetype="dashed")
    raspberry_pi_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_32, x=15, y=(delay_mean_32 + 120),
                                      colour="firebrick")

    raspberry_pi_delay += p9.geom_hline(yintercept=delay_mean_64, colour='darkblue', linetype="dashed")
    raspberry_pi_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_64, x=2, y=(delay_mean_64 - 120),
                                      colour="darkblue")

    prepare_plot(raspberry_pi_delay, '7_raspberry_delay', "Model", 'Czas inferencji [ms]', angle=90)

    # Same but without mobilenet v1 640x640
    raspberry_pi_df = remove_mobilenet_v1(raspberry_pi_df)

    delay_mean_32 = round(
        np.mean(list(raspberry_pi_df.loc[raspberry_pi_df['device'] == 'Raspberry-Pi-Rasbian-32bit']['delay'])), 2)
    delay_mean_64 = round(
        np.mean(list(raspberry_pi_df.loc[raspberry_pi_df['device'] == 'Raspberry-Pi-Ubuntu-64bit']['delay'])), 2)

    raspberry_pi_delay = p9.ggplot(raspberry_pi_df, p9.aes(x='model', y='delay'))
    raspberry_pi_delay += p9.geom_col(p9.aes(fill='device_name'), stat='identity', position='dodge')
    raspberry_pi_delay += p9.scale_fill_discrete(name="Model")

    raspberry_pi_delay += p9.geom_hline(yintercept=delay_mean_32, colour="firebrick", linetype="dashed")
    raspberry_pi_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_32, x=11, y=(delay_mean_32 + 120),
                                      colour="firebrick")

    raspberry_pi_delay += p9.geom_hline(yintercept=delay_mean_64, colour='darkblue', linetype="dashed")
    raspberry_pi_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_64, x=2, y=(delay_mean_64 + 120),
                                      colour="darkblue")

    prepare_plot(raspberry_pi_delay, '7_raspberry_delay_without_m1', 'Model', 'Czas inferencji [ms]', angle=90)


def section8_plots(df_dict):
    # Mean delay and mAP on full integer model on Raspberry Pi Coral
    raspberry_quant_32_df = df_dict['raspberry_quant_32']
    raspberry_quant_64_df = df_dict['raspberry_quant_64']

    delay_mean_32 = round(
        np.mean(
            list(raspberry_quant_32_df.loc[raspberry_quant_32_df['device'] == 'Raspberry-Pi-Rasbian-32bit']['delay'])),
        2)
    delay_mean_64 = round(
        np.mean(
            list(raspberry_quant_64_df.loc[raspberry_quant_64_df['device'] == 'Raspberry-Pi-Ubuntu-64bit']['delay'])),
        2)

    delay_mean_32_coral = round(
        np.mean(list(
            raspberry_quant_32_df.loc[raspberry_quant_32_df['device'] == 'Raspberry-Pi-Rasbian-32bit_Coral']['delay'])),
        2)
    delay_mean_64_coral = round(
        np.mean(list(
            raspberry_quant_64_df.loc[raspberry_quant_64_df['device'] == 'Raspberry-Pi-Ubuntu-64bit_Coral']['delay'])),
        2)

    # Raspberry PI 32 bit - comparison coral with and without
    raspberry_quant_32_df['coral'] = ['Coral TPU' if coral is True else 'Raspberry Pi CPU'
                                      for coral in raspberry_quant_32_df['coral']]

    coral_raspberry_32_delay = p9.ggplot(raspberry_quant_32_df, p9.aes(x='model', y='delay'))
    coral_raspberry_32_delay += p9.geom_col(p9.aes(fill='coral'), stat='identity', position='dodge')
    coral_raspberry_32_delay += p9.scale_fill_discrete(name=" ")

    coral_raspberry_32_delay += p9.geom_hline(yintercept=delay_mean_32_coral, colour='firebrick', linetype="dashed")
    coral_raspberry_32_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_32_coral, x=1,
                                            y=(delay_mean_32_coral + 150),
                                            colour="firebrick")

    coral_raspberry_32_delay += p9.geom_hline(yintercept=delay_mean_32, colour="darkblue", linetype="dashed")
    coral_raspberry_32_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_32, x=4, y=(delay_mean_32 + 150),
                                            colour="darkblue")

    prepare_plot(coral_raspberry_32_delay, '8_coral_raspberry_32_delay', 'Model', 'Czas inferencji [ms]', angle=90)

    # Raspberry PI 64 bit - comparison coral with and without
    raspberry_quant_64_df['coral'] = ['Coral TPU' if coral is True else 'Raspberry Pi CPU'
                                      for coral in raspberry_quant_64_df['coral']]

    coral_raspberry_64_delay = p9.ggplot(raspberry_quant_64_df, p9.aes(x='model', y='delay'))
    coral_raspberry_64_delay += p9.geom_col(p9.aes(fill='coral'), stat='identity', position='dodge')
    coral_raspberry_64_delay += p9.scale_fill_discrete(name=" ")

    coral_raspberry_64_delay += p9.geom_hline(yintercept=delay_mean_64_coral, colour='firebrick', linetype="dashed")
    coral_raspberry_64_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_64_coral, x=1,
                                            y=(delay_mean_64_coral + 150),
                                            colour="firebrick")

    coral_raspberry_64_delay += p9.geom_hline(yintercept=delay_mean_64, colour="darkblue", linetype="dashed")
    coral_raspberry_64_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_64, x=4, y=(delay_mean_64 + 150),
                                            colour="darkblue")

    prepare_plot(coral_raspberry_64_delay, '8_coral_raspberry_64_delay', 'Model', 'Czas inferencji [ms]', angle=90)

    # Same but without mobilenet v1 640x640
    raspberry_quant_32_df = remove_mobilenet_v1(raspberry_quant_32_df)
    raspberry_quant_64_df = remove_mobilenet_v1(raspberry_quant_64_df)

    delay_mean_32 = round(
        np.mean(
            list(raspberry_quant_32_df.loc[raspberry_quant_32_df['device'] == 'Raspberry-Pi-Rasbian-32bit']['delay'])),
        2)
    delay_mean_64 = round(
        np.mean(
            list(raspberry_quant_64_df.loc[raspberry_quant_64_df['device'] == 'Raspberry-Pi-Ubuntu-64bit']['delay'])),
        2)

    delay_mean_32_coral = round(
        np.mean(list(
            raspberry_quant_32_df.loc[raspberry_quant_32_df['device'] == 'Raspberry-Pi-Rasbian-32bit_Coral']['delay'])),
        2)
    delay_mean_64_coral = round(
        np.mean(list(
            raspberry_quant_64_df.loc[raspberry_quant_64_df['device'] == 'Raspberry-Pi-Ubuntu-64bit_Coral']['delay'])),
        2)

    coral_raspberry_32_delay = p9.ggplot(raspberry_quant_32_df, p9.aes(x='model', y='delay'))
    coral_raspberry_32_delay += p9.geom_col(p9.aes(fill='coral'), stat='identity', position='dodge')
    coral_raspberry_32_delay += p9.scale_fill_discrete(name=" ")

    coral_raspberry_32_delay += p9.geom_hline(yintercept=delay_mean_32_coral, colour='firebrick', linetype="dashed")
    coral_raspberry_32_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_32_coral, x=2.25,
                                            y=(delay_mean_32_coral + 40),
                                            colour="firebrick")

    coral_raspberry_32_delay += p9.geom_hline(yintercept=delay_mean_32, colour="darkblue", linetype="dashed")
    coral_raspberry_32_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_32, x=3, y=(delay_mean_32 + 40),
                                            colour="darkblue")
    coral_raspberry_32_delay += p9.coord_cartesian(ylim=(None, 1500))
    prepare_plot(coral_raspberry_32_delay, '8_coral_raspberry_32_delay_without_m1', 'Model', 'Czas inferencji [ms]',
                 angle=90)

    # Raspberry PI 64 bit - comparison coral with and without
    coral_raspberry_64_delay = p9.ggplot(raspberry_quant_64_df, p9.aes(x='model', y='delay'))
    coral_raspberry_64_delay += p9.geom_col(p9.aes(fill='coral'), stat='identity', position='dodge')

    coral_raspberry_64_delay += p9.geom_hline(yintercept=delay_mean_64_coral, colour='firebrick', linetype="dashed")
    coral_raspberry_64_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_64_coral, x=2.25,
                                            y=(delay_mean_64_coral + 25),
                                            colour="firebrick")

    coral_raspberry_64_delay += p9.geom_hline(yintercept=delay_mean_64, colour="darkblue", linetype="dashed")
    coral_raspberry_64_delay += p9.annotate(geom="text", label='μ = %s' % delay_mean_64, x=3, y=(delay_mean_64 + 25),
                                            colour="darkblue")

    coral_raspberry_64_delay += p9.coord_cartesian(ylim=(None, 1500))
    prepare_plot(coral_raspberry_64_delay, '8_coral_raspberry_64_delay_without_m1', 'Model', 'Czas inferencji [ms]',
                 angle=90)


def pareto_plots(df, filename: str, color_process_unit: bool = False, color_architecture: bool = False,
                 fill: str = None):
    optimal_dataframe = get_pareto_optimal(df)

    pareto_line = p9.ggplot(optimal_dataframe, p9.aes(x='delay', y='mAP'))
    pareto_line += p9.geom_line()

    if fill:
        pareto_line += p9.geom_point(p9.aes(fill=fill, size=3))
    else:
        pareto_line += p9.geom_point(p9.aes(size=3))

    pareto_line += p9.scale_size_continuous(guide=False)
    pareto_line += p9.scale_fill_discrete(name=" ")

    x_lim = (min(optimal_dataframe['delay'] - 80), max(optimal_dataframe['delay'] + 160))
    y_lim = (min(optimal_dataframe['mAP']) - 5, max(optimal_dataframe['mAP']) + 5)
    pareto_line += p9.coord_cartesian(xlim=x_lim, ylim=y_lim)

    axes_coefficient = (max(optimal_dataframe['delay']) - min(optimal_dataframe['delay'])) / \
                       (max(optimal_dataframe['mAP']) - min(optimal_dataframe['mAP']))

    sorted_dataframe_rows = sorted(optimal_dataframe.iterrows(), key=lambda x: x[1]['mAP'])
    for index, row in enumerate(sorted_dataframe_rows):
        prev_row = sorted_dataframe_rows[index - 1][1] if index != 0 else None
        row = row[1]
        next_row = sorted_dataframe_rows[index + 1][1] if index != len(sorted_dataframe_rows) - 1 else None

        # Set correct position of displayed text
        prev_unit_vector, next_unit_vector = None, None
        cur_point = row['delay'], row['mAP']
        if prev_row is not None:
            prev_point = prev_row['delay'], prev_row['mAP']
            prev_vector = np.array([cur_point[0] - prev_point[0], cur_point[1] - prev_point[1]])
            prev_unit_vector = prev_vector / np.linalg.norm(prev_vector)

        if next_row is not None:
            next_point = next_row['delay'], next_row['mAP']
            next_vector = np.array([cur_point[0] - next_point[0], cur_point[1] - next_point[1]])
            next_unit_vector = next_vector / np.linalg.norm(next_vector)

        if prev_unit_vector is not None and next_unit_vector is not None:
            unit_vector = np.array([prev_unit_vector[0] + next_unit_vector[0], prev_unit_vector[1] + next_unit_vector[1]])
            unit_vector = unit_vector / np.linalg.norm(unit_vector)
        elif prev_unit_vector is None:
            unit_vector = next_unit_vector
        elif next_unit_vector is None:
            unit_vector = prev_unit_vector
        else:
            unit_vector = [0, 0]

        vector_multiplier = 1.6

        if fill == 'process_unit':
            color = 'firebrick' if row['process_unit'] == 'CPU' else 'darkblue'
        elif fill == 'architecture':
            color = 'firebrick' if row['architecture'] == 'efficientdet' else 'darkblue'
        elif fill == 'quantization':
            color = 'firebrick' if row['quantization'] == 'Full integer' else 'darkblue'
        else:
            color = 'black'

        vector = np.array([unit_vector[0] * axes_coefficient, unit_vector[1]]) * vector_multiplier
        if abs(vector[1]) < 1:
            vector[1] = 1

        if row['architecture'] == 'efficientdet':
            name_size = (x_lim[1] - x_lim[0]) / 7
        else:
            name_size = (x_lim[1] - x_lim[0]) / 5

        name_offset = name_size if unit_vector[0] > 0 else -name_size
        text_x = row['delay'] + vector[0] + name_offset
        text_y = row['mAP'] + vector[1]

        text_x = min(max(text_x, x_lim[0] + name_size), x_lim[1] - name_size)
        text_y = min(max(text_y, y_lim[0]), y_lim[1])

        pareto_line += p9.geom_text(label=row['model'], size=13, x=text_x, y=text_y, colour=color)

    prepare_plot(pareto_line, '_'.join([filename, "pareto"]), 'Czas inferencji [ms]', 'mAP [%]', 90)


def create_plots():
    main_df = create_dataframe()
    df_dict = split_dataframe(main_df)

    # print(main_df.to_string())
    # print(sorted(list(set(list(main_df['model'])))))

    # section0_plots(df_dict)
    # section1_plots(df_dict)
    # section2_plots(df_dict)
    # section3_plots(df_dict)
    # section4_plots(df_dict)
    # section5_plots(df_dict)
    # section6_plots(df_dict)
    # section7_plots(df_dict)
    # section8_plots(df_dict)

    # pareto_plots(df_dict['one_plus_cpu_def'], 'OnePlus-9_CPU', fill='architecture')
    # pareto_plots(df_dict['samsung-A71_cpu_def'], 'Samsung-A71_CPU', fill='architecture')
    # pareto_plots(df_dict['xiaomi-redmi9_cpu_def'], 'Xiaomi-9_CPU', fill='architecture')
    #
    # pareto_plots(df_dict['one_plus_def'], 'OnePlus-9', fill='process_unit')
    # pareto_plots(df_dict['samsung-A71_def'], 'Samsung-A71', fill='process_unit')
    # pareto_plots(df_dict['xiaomi-redmi9_def'], 'Xiaomi-9', fill='process_unit')


    one_plus_cpu_df = df_dict['one_plus_cpu']
    models_quantized = list(set(one_plus_cpu_df.loc[one_plus_cpu_df['quantization'] == 'Full integer']['model_name']))
    model_to_compare = [model in models_quantized for model in one_plus_cpu_df['model_name']]
    one_plus_cpu_df['to_compare'] = model_to_compare
    quantization_one_plus_cpu_df = one_plus_cpu_df.loc[one_plus_cpu_df['to_compare'] == True]

    pareto_plots(quantization_one_plus_cpu_df, 'OnePlus-9_CPU_quantization', fill='quantization')

    one_plus_gpu_df = df_dict['one_plus_gpu']
    models_quantized = list(set(one_plus_gpu_df.loc[one_plus_gpu_df['quantization'] == 'Full integer']['model_name']))
    model_to_compare = [model in models_quantized for model in one_plus_gpu_df['model_name']]
    one_plus_gpu_df['to_compare'] = model_to_compare
    quantization_one_plus_cpu_df = one_plus_gpu_df.loc[one_plus_gpu_df['to_compare'] == True]
    pareto_plots(quantization_one_plus_cpu_df, 'OnePlus-9_GPU_quantization', fill='quantization')



    # for model_name in set(dataframe['model_name']):
    #     for device_name in set(dataframe['device_name']):
    #         model_device_dataframe = dataframe.loc[(dataframe['model_name'] == model_name) &
    #                                                (dataframe['device_name'] == device_name)]
    #
    #         model_device_plot = ggplot(model_device_dataframe, aes(x='model', y='delay')) + geom_col(stat='identity')
    #         model_device_plot += facet_wrap('device')
    #         model_device_plot += theme(axis_text_x=element_text(angle=90, hjust=1))
    #         save_gg_plot(model_device_plot, '_'.join([model_name, device_name]))
    # mobilenets = dataframes_dict['mobilenet']
    # model_dataframe.reset_index(level=0, inplace=True)
    # v1_mobilenets = mobilenets.loc[mobilenets['model_version'] == '1']
    #
    # mobilenets_v1_plot = ggplot(v1_mobilenets, aes(x='model', y='delay'))
    # mobilenets_v1_plot += geom_col(stat='identity')
    # mobilenets_v1_plot += facet_wrap('device')
    # mobilenets_v1_plot += theme(axis_text_x=element_text(angle=90, hjust=1))
    # save_gg_plot(mobilenets_v1_plot)
    #
    # dataframes_dict = split_dataframe(dataframe)
    # # print(data.to_string())
    #
    # everything_scatter_plot = ggplot(dataframe, aes(x='delay', y='mAP', color='model', shape='device')) + geom_point()
    # save_gg_plot(everything_scatter_plot)
    #
    # architecture_scatter_plot = ggplot(dataframe, aes(x='delay', y='mAP', color='architecture',
    #                                                   shape='model_version')) + geom_point()
    # save_gg_plot(architecture_scatter_plot)
    #
    # device_scatter_plot = ggplot(dataframe, aes(x='delay', y='mAP', color='device_type')) + geom_point()
    # save_gg_plot(device_scatter_plot)
    #
    # # Group by Models
    # model_dataframe = dataframe.groupby(['model']).mean()
    # model_dataframe.reset_index(level=0, inplace=True)
    # model_delays_bar_plot = ggplot(model_dataframe, aes(x='model', y='delay')) + geom_col()
    # model_delays_bar_plot += theme(axis_text_x=element_text(angle=70, hjust=1))
    # save_gg_plot(model_delays_bar_plot)
    #
    # model_map_bar_plot = ggplot(model_dataframe, aes(x='model', y='mAP')) + geom_col()
    # model_map_bar_plot += theme(axis_text_x=element_text(angle=70, hjust=1))
    # save_gg_plot(model_map_bar_plot)
    #
    # values = list(model_dataframe['mAP']) + list(model_dataframe['delay'])
    # keys = ['mAP'] * len(model_dataframe['mAP']) + ['delay'] * len(model_dataframe['delay'])
    # models = list(model_dataframe['model']) * 2
    # data = list(zip(models, keys, values))
    # map_delay_df = pd.DataFrame(data=data, columns=['model', 'key', 'value'])
    # model_values_bar_plot = ggplot(map_delay_df, aes(x='model', y='value'))
    # model_values_bar_plot += geom_col(aes(fill='key'), stat='identity', position='dodge')
    # model_values_bar_plot += theme(axis_text_x=element_text(angle=90, hjust=1))
    # model_values_bar_plot += scale_y_log10()
    # save_gg_plot(model_values_bar_plot)
    #
    # # Compare mobilenet quantized and default models
    # quantization_map_col_plot = ggplot(dataframes_dict['mobilenet'], aes(x='quantization', y='mAP'))
    # quantization_map_col_plot += geom_col(aes(fill='model_name'), stat='identity', position='dodge')
    # save_gg_plot(quantization_map_col_plot)  # 978, 1810, 2221
    #
    # quantization_delay_col_plot = ggplot(dataframes_dict['mobilenet'], aes(x='quantization', y='delay'))
    # quantization_delay_col_plot += geom_col(aes(fill='model_name'), stat='identity', position='dodge')
    # save_gg_plot(quantization_delay_col_plot)  # 41, 38, 39


if __name__ == '__main__':
    create_plots()
