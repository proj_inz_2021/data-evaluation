from src.file_support import get_boxes_extension
from parse_to_txt import parse_to_txt
from correct_boxes import correct_boxes
from calculate_map import calculate_map
from create_csv import create_csv
from create_plots import create_plots
from rename_models import rename_models
from calculate_statistics import calculate_statistics
from create_heatmaps import create_heatmaps

if __name__ == '__main__':

    if get_boxes_extension() != 'txt':
        parse_to_txt()
        correct_boxes()

    calculate_map(override=False)
    rename_models()
    create_csv()
    calculate_statistics()
    create_heatmaps()
    create_plots()

