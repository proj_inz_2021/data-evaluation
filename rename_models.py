import os

BASE_PATH = 'devices_results'


def rename_models():
    for device in os.listdir(BASE_PATH):
        device_path = os.path.join(BASE_PATH, device)

        for model in os.listdir(device_path):
            new_model_name = model

            # efficientdet-lite0.tflite
            if '.tflite' in new_model_name:
                new_model_name = new_model_name.replace('.tflite', '')

            # efficientdet-lite0
            if 'mobilenet' in new_model_name:
                new_model_name = new_model_name.replace('mobilenet', 'mn')

            if 'ssd-' in new_model_name:
                new_model_name = new_model_name.replace('ssd-', '')

            if 'efficientdet' in new_model_name:
                new_model_name = new_model_name.replace('efficientdet', 'ed')

            # ed-lite0
            if new_model_name.startswith('ed-'):
                if 'lite0' in new_model_name and '_quant' in new_model_name:
                    index = new_model_name.find('_quant')
                    new_model_name = new_model_name[:index] + '-320x320' + new_model_name[index:]
                elif 'lite0' in new_model_name and not '_quant' in new_model_name:
                    new_model_name = new_model_name + '-320x320'
                elif 'lite1' in new_model_name:
                    new_model_name = new_model_name + '-384x384'
                elif 'lite2' in new_model_name:
                    new_model_name = new_model_name + '-448x448'
                elif 'lite3' in new_model_name:
                    new_model_name = new_model_name + '-512x512'
                elif 'lite4' in new_model_name or 'lite3x' in new_model_name:
                    new_model_name = new_model_name + '-640x640'
            elif new_model_name.startswith('mn-'):
                if 'small' in new_model_name or 'large' in new_model_name:
                    new_model_name = new_model_name + '-320x320'

            if '_edgetpu' in new_model_name:
                new_model_name = new_model_name.replace('_edgetpu', '')

            split_index = 2 if 'no_optimization' in new_model_name else 1 if 'quant' in new_model_name else 0

            if split_index == 0:
                new_model_name = new_model_name.replace('_', '-')
            else:
                new_model_name = '_'.join(['-'.join(new_model_name.split('_')[:-split_index]),
                                           '_'.join(new_model_name.split('_')[-split_index:])])

            if not ('no_optimization' in new_model_name or 'quant' in new_model_name):
                new_model_name = new_model_name + '_default'

            if new_model_name != model:
                os.rename(os.path.join(device_path, model), os.path.join(device_path, new_model_name))


if __name__ == '__main__':
    rename_models()
