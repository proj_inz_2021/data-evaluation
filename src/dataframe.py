from copy import copy
from typing import Dict
import itertools

import oapackage
import numpy as np
import pandas as pd

from .file_support import load_csv


def create_dataframe() -> pd.DataFrame:
    delays_models, delays_devices, delays_data = load_csv('delays.csv')
    map_models, map_devices, map_data = load_csv('mAP.csv')
    assert (delays_models == map_models).all() and (delays_devices == map_devices).all()

    # Device properties
    devices = list(itertools.chain(*[[device] * len(map_models) for device in map_devices]))
    device_types = ['raspberry-pi' if 'Raspberry-Pi' in device else 'smartphone' for device in devices]
    device_name = [''.join(name.split('_')[0]) for name in devices]
    process_unit = ['GPU' if 'GPU' in device else 'CPU' for device in devices]
    coral = ['Coral' in device for device in devices]

    # Model properties
    models = list(itertools.chain(*[map_models] * len(map_devices)))
    model_name = [model.split('_')[0] for model in models]
    architectures = ['efficientdet' if model.startswith('ed') else 'mobilenet' for model in models]
    model_version = [model.split('v')[-1][0] if model.startswith('mn') else model.split('lite')[-1][:2]
                     if 'x-' in model else model.split('lite')[-1][0] for model in models]
    quantization = ['Full integer' if 'quant' in model else 'No optimization' if 'no_optimization' in model else 'Default optimization'
                    for model in models]
    input_size_compare = [True if model in ['mn-v1-300x300_default', 'mn-v1-fpn-640x640_default', 'mn-v2-fpnlite-320x320_default',
                                            'mn-v2-fpnlite-640x640_default'] else False for model in models]

    _640 = [True if '640x640' in model else False for model in models]

    data = list(zip(devices, device_types, device_name, process_unit, coral, models, model_name, architectures, model_version,
                    quantization, input_size_compare, _640, delays_data.flatten(), map_data.flatten()))
    columns = ['device', 'device_type', 'device_name', 'process_unit', 'coral', 'model', 'model_name', 'architecture',
               'model_version', 'quantization', 'input_size_compare', '640', 'delay', 'mAP']

    df = pd.DataFrame(data=data, columns=columns)
    dataframe = df.loc[~df['delay'].isna()]  # Remove rows that value in 'delay' column is NaN

    return dataframe


def split_dataframe(df: pd.DataFrame) -> Dict[str, pd.DataFrame]:
    df_dict: Dict[str, pd.DataFrame] = {}
    df_dict['original'] = df

    # Default models
    df_dict['default_models'] = df.loc[df['quantization'] == 'Default optimization']
    df_dict['default_mobilenet'] = df_dict['default_models'].loc[df_dict['default_models']['architecture'] == 'mobilenet']

    # Default mobilenet on OnePlus
    df_dict['one_plus_def'] = df_dict['default_models'].loc[df_dict['default_models']['device_name'] == 'OnePlus-9']
    df_dict['one_plus_cpu_def'] = df_dict['one_plus_def'].loc[df_dict['one_plus_def']['process_unit'] == 'CPU']
    df_dict['one_plus_gpu_def'] = df_dict['one_plus_def'].loc[df_dict['one_plus_def']['process_unit'] == 'GPU']
    df_dict['one_plus_cpu_mobilenet'] = df_dict['one_plus_cpu_def'].loc[df_dict['one_plus_cpu_def']['architecture'] == 'mobilenet']
    df_dict['one_plus_cpu_efficientdet'] = df_dict['one_plus_cpu_def'].loc[df_dict['one_plus_cpu_def']['architecture'] == 'efficientdet']
    df_dict['one_plus_gpu_mobilenet'] = df_dict['one_plus_gpu_def'].loc[df_dict['one_plus_gpu_def']['architecture'] == 'mobilenet']
    df_dict['one_plus_gpu_efficientdet'] = df_dict['one_plus_gpu_def'].loc[df_dict['one_plus_gpu_def']['architecture'] == 'efficientdet']

    df_dict['one_plus_mobilenet'] = df_dict['one_plus_def'].loc[df_dict['one_plus_def']['architecture'] == 'mobilenet']
    df_dict['one_plus_efficientdet'] = df_dict['one_plus_def'].loc[df_dict['one_plus_def']['architecture'] == 'efficientdet']
    # df_dict['one_plus_cpu_efficientdet'] = df_dict['one_plus_cpu_efficientdet'].sort_values(by=["model_version"])

    # 320x320 and 640x640 v2 Models
    df_dict['default_mobilenet_size_compare'] = df_dict['default_mobilenet'].loc[df_dict['default_mobilenet']['input_size_compare'] == True]
    df_dict['default_mobilenet_v1_size_compare'] = df_dict['default_mobilenet_size_compare'].loc[df_dict['default_mobilenet_size_compare']['model_version'] == '1']
    df_dict['default_mobilenet_v2_size_compare'] = df_dict['default_mobilenet_size_compare'].loc[df_dict['default_mobilenet_size_compare']['model_version'] == '2']

    # GPU vs CPU on OnePlus with default models
    df_dict['default_models_smartphones'] = df_dict['default_mobilenet'].loc[df_dict['default_mobilenet']['device_type'] == 'smartphone']

    # Quantization
    df_dict['one_plus_cpu'] = df.loc[df['device'] == 'OnePlus-9_CPU']
    df_dict['one_plus_gpu'] = df.loc[df['device'] == 'OnePlus-9_GPU']

    # Raspberry PI
    df_dict['raspberry'] = df.loc[df['device_type'] == 'raspberry-pi']
    df_dict['raspberry_without_coral'] = df_dict['raspberry'].loc[df_dict['raspberry']['coral'] == False]
    df_dict['raspberry_quant'] = df_dict['raspberry'].loc[df_dict['raspberry']['quantization'] == 'Full integer']
    df_dict['raspberry_quant_32'] = df_dict['raspberry_quant'].loc[df_dict['raspberry_quant']['device_name'] == 'Raspberry-Pi-Rasbian-32bit']
    df_dict['raspberry_quant_64'] = df_dict['raspberry_quant'].loc[df_dict['raspberry_quant']['device_name'] == 'Raspberry-Pi-Ubuntu-64bit']

    df_dict['samsung-A71_def'] = df_dict['default_models'].loc[df_dict['default_models']['device_name'] == 'Samsung-A71']
    df_dict['samsung-A71_cpu_def'] = df_dict['samsung-A71_def'].loc[df_dict['samsung-A71_def']['process_unit'] == 'CPU']

    df_dict['xiaomi-redmi9_def'] = df_dict['default_models'].loc[df_dict['default_models']['device_name'] == 'Xiaomi-Redmi-Note-9']
    df_dict['xiaomi-redmi9_cpu_def'] = df_dict['xiaomi-redmi9_def'].loc[df_dict['xiaomi-redmi9_def']['process_unit'] == 'CPU']

    assert all([not df_dict[key].empty for key in df_dict.keys()]) # Assert that every created dataframe is not empty
    #     'original': df,
    #
    #     # Based on device
    #     'raspberry': df.loc[df['device_type'] == 'raspberry-pi'],
    #     'raspberry_coral': df.loc[(df['device_type'] == 'raspberry-pi') & (df['coral'] == True)],
    #     'raspberry_without_coral': df.loc[(df['device_type'] == 'raspberry-pi') & (df['coral'] == False)],
    #     'smartphones': df.loc[df['device_type'] == 'smartphone'],
    #     'smartphones_GPU': df.loc[(df['device_type'] == 'smartphone') & (df['GPU'] == True)],
    #     'smartphones_CPU': df.loc[(df['device_type'] == 'smartphone') & (df['GPU'] == False)],
    #
    #     # Based on model
    #     'efficient': df.loc[df['architecture'] == 'efficientdet'],
    #     'mobilenet': df.loc[df['architecture'] == 'mobilenet'],
    #
    #     'full_quantization': df.loc[df['quantization'] == 'full'],
    #     'default_quantization': df.loc[df['quantization'] == 'default'],
    #     'no_quantization': df.loc[df['quantization'] == 'no_optimization']
    #
    #
    # }
    return df_dict


def get_pareto_optimal(df: pd.DataFrame) -> pd.DataFrame:
    data_points = df[['delay', 'mAP']].values.transpose()

    temp_points = copy(data_points)

    for ii in range(0, temp_points.shape[1]):
        temp_points[0, ii] = -temp_points[0, ii]

    pareto = oapackage.ParetoDoubleLong()

    for ii in range(0, temp_points.shape[1]):
        w = oapackage.doubleVector((temp_points[0, ii], temp_points[1, ii]))
        pareto.addvalue(w, ii)

    optimal_indexes = list(pareto.allindices())
    return df.iloc[optimal_indexes]


def remove_mobilenet_v1(df: pd.DataFrame) -> pd.DataFrame:
    return df.loc[df['model_name'] != 'mn-v1-fpn-640x640']
