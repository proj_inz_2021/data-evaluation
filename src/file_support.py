from typing import Optional, Tuple, Union, Dict, Any
from pathlib import Path
import csv
import json
import os
import inspect

from matplotlib import pyplot as plt
from plotnine import ggplot
import numpy as np
import seaborn as sns


BASE_PATH = 'devices_results'
OUTPUT_PATH = 'output'
CSV_PATH = os.path.join(OUTPUT_PATH, 'csv')
PLOT_PATH = os.path.join(OUTPUT_PATH, 'plots')
HEATMAP_PATH = os.path.join(OUTPUT_PATH, 'heatmaps')


def iterate_over_tests():
    for device in os.listdir(BASE_PATH):
        device_path = os.path.join(BASE_PATH, device)
        for model in os.listdir(device_path):
            model_path = os.path.join(device_path, model)
            for test in os.listdir(model_path):
                test_path = os.path.join(model_path, test)
                yield test_path


def get_boxes_extension() -> Optional[str]:
    try:
        sample_test_path = iterate_over_tests().__next__()
    except StopIteration:
        return None

    boxes = os.listdir(os.path.join(sample_test_path, 'boxes'))
    if len(boxes) != 0:
        return boxes[0].split('.')[-1]


def load_json_file(file_path: Union[str, os.PathLike]) -> Dict:
    assert file_path.endswith('.json')
    with open(file_path) as f:
        json_data = json.load(f)
    return json_data


def load_txt_file(file_path: Union[str, os.PathLike]) -> str:
    assert file_path.endswith('.txt')
    with open(file_path) as f:
        txt_data = f.read()
    return txt_data


def save_json(data: Dict, filename: str) -> None:
    with open(os.path.join(OUTPUT_PATH, '.'.join([filename, 'json'])), 'w') as f:
        json.dump(data, f, indent=4)


def load_csv(filename: str, none_value: Any = None) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    with open(os.path.join(CSV_PATH, filename)) as f:
        output = np.array(list(csv.reader(f, delimiter=';')))

    data = np.array([list(map(lambda x: float(x.replace(',', '.')) if x != 'None' else none_value, row))
                     for row in output[1:, 1:]])
    return output[0, 1:], output[1:, 0], data


def save_gg_plot(gg_plot: ggplot, filename: str = None, aspect_ration: float = 0.8):
    filename = _retrieve_name(gg_plot) if filename is None else filename
    Path(PLOT_PATH).mkdir(parents=True, exist_ok=True)
    gg_plot.save('.'.join([os.path.join(PLOT_PATH, filename), 'svg']), width=11.7 * aspect_ration, height=8.27 * aspect_ration)


def save_plt_heatmap(filename: str):
    Path(HEATMAP_PATH).mkdir(parents=True, exist_ok=True)
    plt.savefig(os.path.join(HEATMAP_PATH, '.'.join([filename, 'svg'])), bbox_inches="tight")


def _retrieve_name(var):
    for fi in reversed(inspect.stack()):
        for name in [var_name for var_name, var_val in fi.frame.f_locals.items() if var_val is var]:
            if name != 'ggplot':
                return name
